package com.aggregator.kondor.dao;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;



public interface IKondorDao {


    List getDealTransactions();

    public List getDealTransactions(Date fromDate);
}
