package com.aggregator.app.core.enums;

public class DealTypeEnum {


    public final  static String BUY_FWD="Buy Fwd";
    public final  static String LOAN="L";
    public final  static String BUY="Buy";
    public final  static String DEPOSIT="D";
    public final  static String SELL_FWD="Sell Fwd";
}
