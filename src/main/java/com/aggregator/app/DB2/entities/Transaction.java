package com.aggregator.app.DB2.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "TG_REM_INSTANCE_GROUP")
@Getter
@Setter

public class Transaction {

    @Id
    @Column(name = "TRACKING_POINT_VALUE_ID",
            columnDefinition = "DECIMAL(31,0)")

    Long id;

    @Column(name = "TRANSFER_CURRENCY")
    String currency;

    @Column(name = "TRANSFER_AMOUNT")
    String    amount;

    @Column(name = "VALUE_DATE")
    Date ValueDate;






}
