package com.aggregator.Remmitance.dao.repo;

import com.aggregator.app.DB2.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface IRemmitanceRepo extends JpaRepository<Transaction,Long> {
@Query(     value="select " +
        " TRACKING_POINT_VALUE_ID," + //0
        " TRANSFER_CURRENCY," +//1
        "TRANSFER_AMOUNT, " +//2
        " to_char(VALUE_DATE,'DD-MM-YYYY HH:mm:ss')," +//3
        " NOSTRO_ACC_NUM " +//4
        " from Db2admin.TG_REM_INSTANCE_GROUP" +
        "     " +
        "   where instance_status='Request Completed' " +
        "   and VALUE_DATE between ?1 and ?2 " +
        "order by VALUE_DATE",

        nativeQuery = true)
    List getTransactions(Date from, Date to);


}
