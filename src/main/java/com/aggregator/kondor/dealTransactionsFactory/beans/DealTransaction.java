package com.aggregator.kondor.dealTransactionsFactory.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Setter
@Getter
@Builder

public class DealTransaction {
    private int dealId;
    private int boDealId;
    private int paymentId ;
     private String transactionType;
    private double amount;
     private  String currencyCode;
    private Date valueDate;
    private String accountShortName;
    private String description;
    private String typeOfDeal;
    private Double rate;
    private Date captureDate;
}
