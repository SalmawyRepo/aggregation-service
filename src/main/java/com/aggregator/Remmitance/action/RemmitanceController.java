package com.aggregator.Remmitance.action;

import com.aggregator.Remmitance.service.IRemmitanceService;
import com.aggregator.kondor.service.IKondorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
class RemmitanceController {

    @Autowired
    private IRemmitanceService service;

    @RequestMapping(value = "/getRemitance", method = RequestMethod.GET)

    public ResponseEntity<List> getRemitance() {

        Calendar c=Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH,1);
        c.set(Calendar.MONTH,2);
        c.set(Calendar.YEAR,2020);

        Date from=new Date(c.getTimeInMillis());

        c.set(Calendar.DAY_OF_MONTH,30);

        Date to=new Date(c.getTimeInMillis());

        return ResponseEntity.ok().body(service.getTransactions(from,to));
    }


    @RequestMapping(value = "/getRemitanceData", method = RequestMethod.GET)


    public ResponseEntity<List> getRemitanceData(@RequestParam("from") @DateTimeFormat(pattern="dd-MM-yyyy HH:mm:ss") Date fromDate,
                                                 @RequestParam("to") @DateTimeFormat(pattern="dd-MM-yyyy HH:mm:ss") Date toDate) {

       log.info("from date  =>"+fromDate);
        log.info("to date  =>"+toDate);
        List data=service.getTransactions(fromDate,toDate);
        return ResponseEntity.ok().body(data);
    }



}
