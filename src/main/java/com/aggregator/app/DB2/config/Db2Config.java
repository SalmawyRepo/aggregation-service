package com.aggregator.app.DB2.config;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "db2EntityManager",
        transactionManagerRef = "db2TransactionManager",
        basePackages = "com.aggregator.Remmitance.dao"
)
public class Db2Config {

    @Bean
    @ConfigurationProperties(prefix = "spring.db2.datasource")
    public DataSource db2DataSource() {


        return DataSourceBuilder
                .create()
                .build();
    }

    @Bean(name = "db2EntityManager")
    public LocalContainerEntityManagerFactoryBean db2EntityManagerFactory(EntityManagerFactoryBuilder builder) {

       DataSource dataSource=db2DataSource();



        return builder
                .dataSource(dataSource)
                .properties(hibernateProperties())
                .packages("com.aggregator.app.DB2.entities")
                .persistenceUnit("db2PU")
                .build();
    }

    @Bean(name = "db2TransactionManager")
    public PlatformTransactionManager mysqlTransactionManager(@Qualifier("db2EntityManager") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);//
    }

    private Map<String, Object> hibernateProperties() {

        Resource resource = new ClassPathResource("hibernate.properties");

        try {
            Properties properties = PropertiesLoaderUtils.loadProperties(resource);

            Map<String, Object> collect = properties.entrySet().stream()
                    .collect(Collectors.toMap(
                            e -> e.getKey().toString(),
                            Map.Entry::getValue)
                    );

            collect.put("hibernate.dialect","org.hibernate.dialect.DB2Dialect");
//            collect.put("hibernate.hbm2ddl.auto", "update");
            return collect;
        } catch (IOException e) {
            return new HashMap<String, Object>();
        }
    }
}

