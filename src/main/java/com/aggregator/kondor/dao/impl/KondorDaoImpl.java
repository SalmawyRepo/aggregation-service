package com.aggregator.kondor.dao.impl;

import com.aggregator.kondor.dao.IKondorDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Repository
@Slf4j
public class KondorDaoImpl implements IKondorDao {


    @Value("${kondor.deals.query.noCondition}")
    String noConditionQuery;
    @Value("${kondor.deals.query.fromToDate}")
    String fromToDateQuery;

    @PersistenceContext
    @Qualifier("sqlEntityManager")
    EntityManager sqlEntityManager;



    @Override
    public List getDealTransactions() {

        log.info(noConditionQuery);

        Query query=sqlEntityManager.createNativeQuery(noConditionQuery);
        return   query.getResultList();



    }

    @Override
    public List getDealTransactions(Date fromDate) {

        Query query=sqlEntityManager.createNativeQuery(fromToDateQuery)
                .setParameter("fromDate",fromDate, TemporalType.DATE);
               /// .setParameter("toDate",toDate, TemporalType.DATE);
        return   query.getResultList();
     //   return new ArrayList();
            }
}
