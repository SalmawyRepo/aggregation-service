package com.aggregator.kondor.service;

import com.aggregator.kondor.dealTransactionsFactory.beans.DealTransaction;

import java.util.Date;
import java.util.List;

public interface IKondorService {


    List getDeals();

    List <DealTransaction> getDealsTransactions( Date from);

}
