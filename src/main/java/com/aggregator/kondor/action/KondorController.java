package com.aggregator.kondor.action;

import com.aggregator.kondor.service.IKondorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class KondorController {

    @Autowired
    private IKondorService service;




/*
  @RequestMapping(value = "/getAccountDeals", method = RequestMethod.GET)

    public ResponseEntity<List> getRemitanceData(@RequestParam("cptyId")String cptyId,
                                                 @RequestParam("from") @DateTimeFormat(pattern="dd-MM-yyyy HH:mm:ss") Date fromDate,
                                                 @RequestParam("to") @DateTimeFormat(pattern="dd-MM-yyyy HH:mm:ss") Date toDate) {
        return ResponseEntity.ok().body(service.getDeals(cptyId,fromDate,toDate));
    }*/


  @RequestMapping(value = "/getKondorAllDeals", method = RequestMethod.GET)
    @Transactional
    public ResponseEntity<List> getKondorAccountDeals() {

      return ResponseEntity.ok().body(service.getDeals());
    }




    @RequestMapping(value = "/getKondorDeals", method = RequestMethod.GET)
    @Transactional
    public ResponseEntity<List> getKondorDeals(@RequestParam("from") @DateTimeFormat(pattern="dd-MM-yyyy HH:mm:ss") Date fromDate) {

        return ResponseEntity.ok().body(service.getDealsTransactions(fromDate));
    }
}

