package com.aggregator.aggregationService.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppServiceController {

    @RequestMapping(path= "/isServiceUp")
    public ResponseEntity isServiceUp(){


        return ResponseEntity.ok().body("OK");
    }

}
