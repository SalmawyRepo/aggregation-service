package com.aggregator.Remmitance.command;


import com.aggregator.Remmitance.dao.repo.IRemmitanceRepo;
import com.aggregator.Remmitance.service.IRemmitanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Slf4j
//@Component
public class RemmitanceCommandLine implements CommandLineRunner {

@Autowired
    IRemmitanceService service;

    @Autowired
    IRemmitanceRepo repo;
      @Override
    public void run(String... args) throws Exception {
          Calendar c=Calendar.getInstance();
          c.set(Calendar.DAY_OF_MONTH,1);
          c.set(Calendar.MONTH,2);
          c.set(Calendar.YEAR,2020);

          Date from=new Date(c.getTimeInMillis());

          c.set(Calendar.DAY_OF_MONTH,30);

          Date to=new Date(c.getTimeInMillis());

           List result=repo.getTransactions(from,to);


    }
}
