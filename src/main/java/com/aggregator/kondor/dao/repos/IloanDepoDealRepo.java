package com.aggregator.kondor.dao.repos;

public interface IloanDepoDealRepo /*extends JpaRepository<LoanDepoDeal,Integer>*/ {
/*

    String EGPCurrencyId="52030";



    @Query(value = "SELECT \n" +
            "         payTransaction.Payment_Id AS Payment_Id, \n" +
            "\t\t     payTransaction.BODeal_Id AS BODealId, \n" +
            "       \t\t    substring(acct.BankAccount_ShortName , 0, 8) AS Account_ShortName,\n" +
            "\t\t     payTransaction.Sign AS Sign, \n" +
            "         payTransaction.Amount AS Amount,      \n" +
            "         payTransaction.ValueDate as ValueDate,\n" +
            "\t\t \t  CASE \n" +
            "            WHEN substring(CAST(accy.Currencies_ShortName AS nvarchar(max)), 3, 1) = 'X' THEN occy.Currencies_ShortName\n" +
            "            ELSE accy.Currencies_ShortName\n" +
            "         END AS Currencies_ShortName,\n" +
            "\t\t   payTransaction.Description AS Description\n" +
            "\t\t \n" +
            "\t\t  \n" +
            "      FROM \n" +


            "          kplustp..Payment  AS payTransaction \n" +
            "            LEFT JOIN kplustp..BankAccount  AS acct \n" +
            "            ON acct.BankAccount_Id = payTransaction.BankAccount_Id_Cpty \n" +
            "            LEFT JOIN kplus..Currencies  AS accy \n" +
            "            ON accy.Currencies_Id = payTransaction.Currencies_Id \n" +
            "            LEFT OUTER JOIN kplus..BasketCurrencies  AS bc \n" +
            "            ON bc.Currencies_Id = accy.Currencies_Id \n" +
            "            LEFT JOIN kplus..Currencies  AS occy \n" +
            "            ON occy.Currencies_Id = bc.Currencies_Id_Aggregation\n" +
            "\t\t\tinner join kplustp..Folders as folder\n" +
            "\t\t\tON folder.Folders_Id=payTransaction.Folders_Id\n" +
            "    \n" +
            "\tWHERE 1=1\n" +
            "\t\n" +
            " \t\tand payTransaction.Currencies_Id <>52030\n" +
            "\tand folder.Folders_ShortName  in \n" +
            "('E_FX_IB',\n" +
            "'E_FX_TRD',\n" +
            "'E_FX_FWD',\n" +
            "'E_FX_SWAP',\n" +
            "'E_MM_CBEPT',\n" +
            "'E_ARBITRAG',\n" +
            "'E_BILATERA',\n" +
            "'E_BRIDGE',\n" +
            "'E_FLOATING',\n" +
            "'E_SYN_LOAN',\n" +
            "'E_MM_TRD',\n" +
            "'FX_PAY_IN',\n" +
            "'FX_PAY_OUT',\n" +
            "'FX_IN',\n" +
            "'FX_OUT',\n" +
            "'E_FX_FI',\n" +
            "'E_FX_FI_I',\n" +
            "'E_FX_FI_T',\n" +
            "'E_FX_SWAP_I')\n" +
            " and payTransaction.ValueDate between ?1 and ?2" ,nativeQuery = true)
    List getDealTransactions( Date from, Date to);

    @Query(value = "SELECT \n" +
            "         payTransaction.Payment_Id AS Payment_Id, \n" +
            "\t\t     payTransaction.BODeal_Id AS BODealId, \n" +
            "       \t\t   substring(acct.BankAccount_ShortName , 0, 8) AS Account_ShortName,\n" +
            "\t\t     payTransaction.Sign AS Sign, \n" +
            "         payTransaction.Amount AS Amount,      \n" +
            "         payTransaction.ValueDate as ValueDate,\n" +
            "\t\t \t  CASE \n" +
            "            WHEN substring(CAST(accy.Currencies_ShortName AS nvarchar(max)), 3, 1) = 'X' THEN occy.Currencies_ShortName\n" +
            "            ELSE accy.Currencies_ShortName\n" +
            "         END AS Currencies_ShortName,\n" +
            "\t\t   payTransaction.Description AS Description \n" +
            "\t\t \n" +
            "\t\t  \n" +
            "      FROM \n" +
            "          kplustp..Payment  AS payTransaction \n" +
            "            LEFT JOIN kplustp..BankAccount  AS acct \n" +
            "            ON acct.BankAccount_Id = payTransaction.BankAccount_Id_Cpty \n" +
            "            LEFT JOIN kplus..Currencies  AS accy \n" +
            "            ON accy.Currencies_Id = payTransaction.Currencies_Id \n" +
            "            LEFT OUTER JOIN kplus..BasketCurrencies  AS bc \n" +
            "            ON bc.Currencies_Id = accy.Currencies_Id \n" +
            "            LEFT JOIN kplus..Currencies  AS occy \n" +
            "            ON occy.Currencies_Id = bc.Currencies_Id_Aggregation\n" +
            "\t\t\tinner join kplustp..Folders as folder\n" +
            "\t\t\tON folder.Folders_Id=payTransaction.Folders_Id\n" +
            "    \n" +
            "\tWHERE 1=1\n" +
            "\t\n" +
            " \t\tand payTransaction.Currencies_Id <>52030\n" +
            "\tand folder.Folders_ShortName  in \n" +
            "('E_FX_IB',\n" +
            "'E_FX_TRD',\n" +
            "'E_FX_FWD',\n" +
            "'E_FX_SWAP',\n" +
            "'E_MM_CBEPT',\n" +
            "'E_ARBITRAG',\n" +
            "'E_BILATERA',\n" +
            "'E_BRIDGE',\n" +
            "'E_FLOATING',\n" +
            "'E_SYN_LOAN',\n" +
            "'E_MM_TRD',\n" +
            "'FX_PAY_IN',\n" +
            "'FX_PAY_OUT',\n" +
            "'FX_IN',\n" +
            "'FX_OUT',\n" +
            "'E_FX_FI',\n" +
            "'E_FX_FI_I',\n" +
            "'E_FX_FI_T',\n" +
            "'E_FX_SWAP_I')\n" +
            "   " ,nativeQuery = true)
    List getAllDealsTransactions();
*/


}
