package com.aggregator.Remmitance.service.impl;


import com.aggregator.Remmitance.dao.repo.IRemmitanceRepo;
import com.aggregator.Remmitance.service.IRemmitanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service
public class RemmitanceService implements IRemmitanceService {


    @Autowired
    IRemmitanceRepo remmitanceDao;


    @Override
    public List getTransactions(Date from, Date to) {



        return remmitanceDao.getTransactions(  from,   to);
    }
}
