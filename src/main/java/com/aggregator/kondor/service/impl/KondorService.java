package com.aggregator.kondor.service.impl;

import com.aggregator.app.core.enums.TransactionTypeEnum;
import com.aggregator.kondor.dao.IKondorDao;
import com.aggregator.kondor.dealTransactionsFactory.beans.DealTransaction;
import com.aggregator.kondor.service.IKondorService;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class KondorService implements IKondorService {

@Autowired
IKondorDao baseDao;

    @Autowired
   private IKondorDao kondorDao;


  /*  @Autowired
    private IloanDepoDealRepo loanDepoRepo;

*/



    @Override
    public List getDeals() {

        List<Object[]>  deals=kondorDao.getDealTransactions();



        return mapResulttoObject(deals);
    }



private  List <DealTransaction> mapResulttoObject(List<Object[]> raws){
        return (raws).stream().map(raw -> DealTransaction.builder()
            .paymentId((Integer) raw[0])
            .boDealId((Integer) raw[1])
            .accountShortName((String) raw[2])
            .transactionType((Integer.parseInt(raw[3].toString())<1)? TransactionTypeEnum.Debit :TransactionTypeEnum.Credit)
            .amount(Math.abs((Double) raw[4]))
             .valueDate((Date) raw[5])
            .description((String) raw[7])
            .currencyCode((String) raw[6])
            .typeOfDeal((String) raw[8])
            .dealId( Integer.parseInt((raw[9].toString()).split("-")[1]) )
            .rate((Double)raw[10])
                .captureDate((Date) raw[11])
            .build()
    ).collect(Collectors.toList());



}


    @Override
    public  List <DealTransaction> getDealsTransactions( Date from) {


        List<Object[]>  deals=kondorDao.getDealTransactions(from);
        return mapResulttoObject(deals);
    }

}

