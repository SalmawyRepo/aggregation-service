package com.aggregator.app.core.enums;

public class DealNameEnum {
    public final  static String LOAN_DEPO ="Loan_Depo";
    public final  static String IAM_DISCOUNTED ="IAM_Discounted";
    public final  static String FORWARD="Forward";
    public final  static String FX_SWAP="Fx_Swap";
    public final  static String SPOT ="Spot";
}
